# static-files

A docker container that serves static files. As simple as that. [Example](https://files.simonbreiter.com).

## How to use

Builds and starts docker container.

```bash
docker-compose build; docker-compose up -d
```

